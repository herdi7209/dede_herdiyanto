package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.BotolModel;

public interface BotolRepository extends JpaRepository<BotolModel, String>{

	@Query("SELECT B FROM BotolModel B WHERE B.kodeBotol = ?1")
	BotolModel cariKodeBotol(String kodeBotol);
}
