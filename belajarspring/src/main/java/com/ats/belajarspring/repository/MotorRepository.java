package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.MotorModel;

public interface MotorRepository extends JpaRepository<MotorModel, String>{

}
