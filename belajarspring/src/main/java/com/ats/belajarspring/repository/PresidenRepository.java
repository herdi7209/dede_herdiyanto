package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, String>{

	@Query("SELECT P FROM PresidenModel P WHERE P.namaPresiden LIKE '%b%' AND P.usiaPresiden = 82")
	List<PresidenModel> cariUsia();
}
