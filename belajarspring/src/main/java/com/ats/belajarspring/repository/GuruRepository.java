package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.GuruModel;

public interface GuruRepository extends JpaRepository<GuruModel, String>{

}
