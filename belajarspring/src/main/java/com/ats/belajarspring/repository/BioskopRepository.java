package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.BioskopModel;

public interface BioskopRepository extends JpaRepository<BioskopModel, String>{

}
