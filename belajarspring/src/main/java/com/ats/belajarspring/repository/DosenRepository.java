package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.DosenModel;


public interface DosenRepository extends JpaRepository<DosenModel, String>{
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen DESC")
	List<DosenModel> querySelectAllOrderNamaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.namaDosen ASC")
	List<DosenModel> querySelectAllOrderNamaAsc();

	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen DESC")
	List<DosenModel> querySelectAllOrderUsiaDesc();
	
	@Query("SELECT D FROM DosenModel D ORDER BY D.usiaDosen ASC")
	List<DosenModel> querySelectAllOrderUsiaAsc();
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen LIKE %?1%")
	List<DosenModel> querySelectAllWhereNamaLike(String katakunci_nama);
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen = ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaSama(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen > ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaLebih(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaKurang(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen >= ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaLebihSama(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen <= ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaKurangSama(int katakunci_usia, String awalan_nama);
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen != ?1 AND D.namaDosen LIKE ?2%")
	List<DosenModel> queryCariUsiaTidakSama(int katakunci_usia, String awalan_nama);
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	@Query("SELECT D FROM DosenModel D WHERE D.namaDosen = ?1")
	DosenModel queryWhereID(String namaDosen);
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	@Query("SELECT D FROM DosenModel D WHERE D.usiaDosen < 25")
	List<DosenModel> usiaLessDuaLima();

	@Query("SELECT D FROM DosenModel D Where D.usiaDosen > 60")
	List<DosenModel> usiaMoreEnamPuluh();
	
}
