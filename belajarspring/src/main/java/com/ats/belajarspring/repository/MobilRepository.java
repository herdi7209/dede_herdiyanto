package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.MobilModel;

public interface MobilRepository extends JpaRepository<MobilModel, String>{

	@Query("SELECT M FROM MobilModel M WHERE M.kodeMobil = ?1")
	MobilModel cariKodeMobil(String kodeMobil);
	
	@Query("SELECT M FROM MobilModel M ORDER BY M.namaMobil ASC")
	List<MobilModel> urutNamaMobil();
}
