package com.ats.belajarspring.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.KarakterModel;

public interface KarakterRepository extends JpaRepository<KarakterModel, String> {

	@Query("SELECT K FROM KarakterModel K WHERE K.namaKarakter = ?1")
	KarakterModel searchNamaKarakter(String namaKarakter);
}
