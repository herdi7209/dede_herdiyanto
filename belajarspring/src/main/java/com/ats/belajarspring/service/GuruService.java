package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.GuruModel;
import com.ats.belajarspring.repository.GuruRepository;

@Service
@Transactional
public class GuruService {

	@Autowired
	private GuruRepository guruRepository;
	
	public void create(GuruModel guruModel) {
		this.guruRepository.save(guruModel);
	}
	
	public List<GuruModel> read(){
		return this.guruRepository.findAll();
	}
}
