package com.ats.belajarspring.service;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.MotorModel;
import com.ats.belajarspring.repository.MotorRepository;

@Service
@Transactional
public class MotorService {

	@Autowired
	private MotorRepository motorRepository;
	
	public void create(MotorModel motorModel) {
		motorRepository.save(motorModel);
	}
	
	public List<MotorModel> read(){
		return motorRepository.findAll();
	}
}
