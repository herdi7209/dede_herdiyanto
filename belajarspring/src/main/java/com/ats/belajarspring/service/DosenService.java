package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.repository.DosenRepository;

@Service
@Transactional
public class DosenService {
	
	@Autowired
	private DosenRepository dosenRepository;

	// modifier kosong/g namaMtehod(TipeVariable namaVariable)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	public List<DosenModel> read() {
		return this.dosenRepository.findAll();
	}
	
	public List<DosenModel> readOrderNama() {
		return this.dosenRepository.querySelectAllOrderNamaDesc();
	}
	
	public List<DosenModel> readOrderBy() {
		return this.dosenRepository.querySelectAllOrderUsiaDesc();
	}
	
	public List<DosenModel> readWhereNama(String katakunci_nama) {
		return this.dosenRepository.querySelectAllWhereNamaLike(katakunci_nama);
	}
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	public List<DosenModel> readWhereUsiaByOperator(String operator, String awalan_nama, int katakunci_usia){
		 
		if (operator.equals("=")) {
			return this.dosenRepository.queryCariUsiaSama(katakunci_usia, awalan_nama);
		}else if (operator.equals(">")) {
			return this.dosenRepository.queryCariUsiaLebih(katakunci_usia, awalan_nama);
		}else if (operator.equals("<")) {
			return this.dosenRepository.queryCariUsiaKurang(katakunci_usia, awalan_nama);
		}else if (operator.equals(">=")) {
			return this.dosenRepository.queryCariUsiaLebihSama(katakunci_usia, awalan_nama);
		}else if (operator.equals("<=")) {
			return this.dosenRepository.queryCariUsiaKurangSama(katakunci_usia, awalan_nama);
		}else {
			return this.dosenRepository.queryCariUsiaTidakSama(katakunci_usia, awalan_nama);
		}

	}
	
	public DosenModel readByID(String namaDosen) {
		return this.dosenRepository.queryWhereID(namaDosen);
	}
	
	public void delete(String namaDosen) {
		dosenRepository.deleteById(namaDosen);
	}
	
	public void update(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
//	public void deleteApi(DosenModel dosenModel) {
//		dosenRepository.delete(dosenModel);
//	}
	
	public List<DosenModel> usiaLessDuaLima(){
		return dosenRepository.usiaLessDuaLima();
	}
	
	public List<DosenModel> usiaMoreThanLimaPuluh(){
		return dosenRepository.usiaMoreEnamPuluh();
	}
		
}
