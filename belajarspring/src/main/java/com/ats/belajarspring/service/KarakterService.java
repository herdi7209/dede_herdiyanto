package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.KarakterModel;
import com.ats.belajarspring.repository.KarakterRepository;

@Service
@Transactional
public class KarakterService {

	@Autowired
	private KarakterRepository karakterRepository;
	
	public void create(KarakterModel karakterModel) {
		karakterRepository.save(karakterModel);
	}
	
	public List<KarakterModel> read() {
		return this.karakterRepository.findAll();
	}
	
	public void update(KarakterModel karakterModel){
		this.karakterRepository.save(karakterModel);
	}
	
	public void delete(KarakterModel karakterModel) {
		this.karakterRepository.delete(karakterModel);
	}
}
