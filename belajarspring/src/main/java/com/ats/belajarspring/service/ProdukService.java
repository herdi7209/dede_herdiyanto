package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.repository.ProdukRepository;

@Service
@Transactional
public class ProdukService {

	@Autowired
	private ProdukRepository produkRepository;
	
	public void create(ProdukModel produkModel) {
		this.produkRepository.save(produkModel);
	}
	
	public List<ProdukModel> tampilProduk(){
		return produkRepository.findAll();
	}
	
	public List<ProdukModel> cariNamaProduk(String namaProduk){
		return produkRepository.cariSemuaDataProduk(namaProduk);
	} 
	
	public List<ProdukModel> cariKode(String kodeProduk){
		return produkRepository.cariKodeProduk(kodeProduk);
	}
	
	public List<ProdukModel> cariHargaASC(String hargaProduk){
		return produkRepository.cariHargaProdukASC(hargaProduk);
	} 
}
