package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.BioskopModel;
import com.ats.belajarspring.repository.BioskopRepository;

@Service
@Transactional
public class BioskopService {
	
	@Autowired
	private BioskopRepository bioskopRepository;

	public void create(BioskopModel bioskopModel) {
		bioskopRepository.save(bioskopModel);
	}
	
	public List<BioskopModel> read(){
		return this.bioskopRepository.findAll();
	}
}
