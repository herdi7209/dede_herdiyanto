package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.MobilModel;
import com.ats.belajarspring.repository.MobilRepository;

@Service
@Transactional
public class MobilService {

	@Autowired
	private MobilRepository mobilRepository;
	
	public void create(MobilModel mobilModel) {
		this.mobilRepository.save(mobilModel);
	}
	
	public List<MobilModel> read(){
		return this.mobilRepository.findAll();
	}
	
	public void update(MobilModel mobilModel) {
		mobilRepository.save(mobilModel);
	}
	
	public void delete(MobilModel mobilModel) {
		mobilRepository.delete(mobilModel);
	}
	

}
