package com.ats.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrisaktiController {

	@RequestMapping(value="daftar")
	public String methodDaftar() {
		String html ="trisakti/daftar";
		return html;
	}
	//nyoba comment
	@RequestMapping(value="hasil")
	public String methodHasil(HttpServletRequest request, Model model) {
		String nama = request.getParameter("daftar_nama");
		String tempat = request.getParameter("daftar_tempat");
		String tanggal = request.getParameter("daftar_tanggal");
		String jk = request.getParameter("daftar_jk");
		String agama = request.getParameter("daftar_agama");
		String telp = request.getParameter("daftar_telp");
		String hp = request.getParameter("daftar_hp");
		String kewarganegaraan = request.getParameter("daftar_kwn");
		String nik = request.getParameter("daftar_nik");
		String goldarah = request.getParameter("daftar_goldarah");
		String anakke = request.getParameter("daftar_anakke");
		String status = request.getParameter("daftar_status");
		
		
		System.out.println(nama);
		System.out.println(tempat);
		System.out.println(tanggal);
		System.out.println(jk);
		System.out.println(agama);
		System.out.println(telp);
		System.out.println(hp);
		System.out.println(kewarganegaraan);
		System.out.println(nik);
		System.out.println(goldarah);
		System.out.println(anakke);
		System.out.println(status);
		
		model.addAttribute("trisakti_nama", nama);
		model.addAttribute("trisakti_tempat", tempat);
		model.addAttribute("trisakti_tanggal", tanggal);
		model.addAttribute("trisakti_jk", jk);
		model.addAttribute("trisakti_agama", agama);
		model.addAttribute("trisakti_telp", telp);
		model.addAttribute("trisakti_hp", hp);
		model.addAttribute("trisakti_kewarganegaraan", kewarganegaraan);
		model.addAttribute("trisakti_nik", nik);
		model.addAttribute("trisakti_goldarah", goldarah);
		model.addAttribute("trisakti_anakke", anakke);
		model.addAttribute("trisakti_status", status);
		
		String html ="trisakti/hasil";
		return html;
	}
	
}
