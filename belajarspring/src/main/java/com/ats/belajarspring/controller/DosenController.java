package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.service.DosenService;

@Controller
public class DosenController {

	@Autowired // Pengganti Instance = IOC / DI
	private DosenService dosenService;
	
	@RequestMapping(value="tambah_dosen")
	public String menuIsiDosen() {
		String html = "dosen/isi_dosen";
		return html;
	}
	
	@RequestMapping(value="satu_dosen")
	public String menuSatuDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		// transaksi Simpan, C create
		dosenService.create(dosenModel);

		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value="list_dosen")
	public String menuListDosen(Model model) {
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value="hasil_banyak_dosen")
	public String menuBanyakDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		// transaksi Simpan, C create
		dosenService.create(dosenModel);
		
		dosenModelList = this.dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value="banyak_dosen_order_nama")
	public String menuBanyakDosenOrderNama(HttpServletRequest request, Model model) {
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen_order_nama";
		return html;
	}
	
	@RequestMapping(value="order_dosen")
	public String menuOrderDosen(HttpServletRequest request, Model model) {
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/order_dosen";
		return html;
	}
	
	@RequestMapping(value="cari_nama_dosen")
	public String menuCariNamaDosen(HttpServletRequest request, Model model) {
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_cari_nama")
	public String menuProsesCariNamaDosen(HttpServletRequest request, Model model) {
		String katakunci_nama = request.getParameter("katakunci_nama");
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readWhereNama(katakunci_nama);
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value="cari_usia")
	public String menuCariUsia(HttpServletRequest request, Model model) {
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value="proses_cari_usia")
	public String menuProsesCariUsia(HttpServletRequest request, Model model) {
		int katakunci_usia = Integer.valueOf(request.getParameter("katakunci_usia"));
		String operator = request.getParameter("operator");
		String awalan_nama = request.getParameter("awalan_nama");
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.readWhereUsiaByOperator(operator, awalan_nama, katakunci_usia);		
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value="ubah_dosen")
	public String menuUbahDosen(HttpServletRequest request, Model model) {
		String namaID= request.getParameter("namaID");
		
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/ubah_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_ubah_dosen")
	public String menuProsesUbahDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("namaID");
		int usiaDosen = Integer.valueOf(request.getParameter("usiaID"));
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		// transaksi Simpan, C create
		dosenService.update(dosenModel);
		
		dosenModelList = this.dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	
	@RequestMapping(value="hapus_dosen")
	public String menuHapusDosen(HttpServletRequest request, Model model) {
		String namaID= request.getParameter("namaID");
		
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/hapus_dosen";
		return html;
	}
	
	@RequestMapping(value="proses_hapus_dosen")
	public String menuProsesHapusDosen(HttpServletRequest request, Model model) {
		String namaDosen = request.getParameter("namaDosen");
		
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		
		dosenService.delete(namaDosen);
		
		dosenModelList = this.dosenService.read();
		model.addAttribute("dosenModelList", dosenModelList);
		String html="dosen/banyak_dosen";
		return html;
		
	}
}
