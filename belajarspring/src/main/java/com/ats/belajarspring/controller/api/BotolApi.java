package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.BotolModel;
import com.ats.belajarspring.repository.BotolRepository;
import com.ats.belajarspring.service.BotolService;

@RestController
@RequestMapping(value="api/BotolApi")
public class BotolApi {

	@Autowired
	private BotolService botolService;
	
	@Autowired
	private BotolRepository botolRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code=HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody BotolModel botolModel){
		botolService.create(botolModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Simpan");
		return map;
	}
	
	@GetMapping("get")
	public List<BotolModel> getApi(){
		List<BotolModel> botolModelList = new ArrayList<BotolModel>();
		botolModelList = botolService.cariBotol();
		return botolModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code=HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody BotolModel botolModel){
		botolService.update(botolModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Ubah");
		return map;
	}
	
	@DeleteMapping("delete/{kodeBotol}")
	@ResponseStatus(code=HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String kodeBotol){
		botolService.delete(botolRepository.cariKodeBotol(kodeBotol));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Hapus");
		return map;
	}
}
