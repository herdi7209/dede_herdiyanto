package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.GuruModel;
import com.ats.belajarspring.service.GuruService;

@Controller
public class GuruController {

	@Autowired
	private GuruService guruService;
	
	@RequestMapping(value="menu_guru")
	public String menuGuru() {
		String html="guru/menu_guru";
		return html;
	}
	
	@RequestMapping(value="proses_tampil_guru")
	public String menuTampilGuru(HttpServletRequest request, Model model) {
		int kodeGuru = Integer.valueOf(request.getParameter("kodeGuru"));
		String namaGuru = request.getParameter("namaGuru");
		String jurusanGuru = request.getParameter("jurusanGuru");
		
		GuruModel guruModel = new GuruModel();
		guruModel.setKodeGuru(kodeGuru);
		guruModel.setNamaGuru(namaGuru);
		guruModel.setJurusanGuru(jurusanGuru);
		
		model.addAttribute("guruModel", guruModel);
		
		String html="guru/tampil_guru";
		return html;
	}
	
	@RequestMapping(value="proses_tampil_guru_list")
	public String menuTampilGuruList(HttpServletRequest request, Model model) {
		int kodeGuru = Integer.valueOf(request.getParameter("kodeGuru"));
		String namaGuru = request.getParameter("namaGuru");
		String jurusanGuru = request.getParameter("jurusanGuru");
		
		List<GuruModel> guruModelList = new ArrayList<GuruModel>();
		
		GuruModel guruModel = new GuruModel();
		guruModel.setKodeGuru(kodeGuru);
		guruModel.setNamaGuru(namaGuru);
		guruModel.setJurusanGuru(jurusanGuru);
		
		guruService.create(guruModel);
		
		guruModelList = this.guruService.read();
		
		model.addAttribute("guruModelList", guruModelList);
		
		String html="guru/tampil_guru_list";
		return html;
	}
}
