package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.KarakterModel;
import com.ats.belajarspring.service.KarakterService;

@Controller
public class KarakterController {
	
	@Autowired
	private KarakterService karakterService;
	
	@RequestMapping(value="tambah_karakter")
	public String menuTambahKarakter () {
		String html = "karakter/daftar_karakter";
		return html;
	}
	
	@RequestMapping(value="hasil_daftar")
	public String menuHasilTambahKarakter(HttpServletRequest request, Model model) {
		String namaKarakter = request.getParameter("nama");
		int levelKarakter = Integer.valueOf(request.getParameter("level"));
		String statusKarakter = request.getParameter("status");
		
		List<KarakterModel> karakterModelList = new ArrayList<KarakterModel>();
		
		KarakterModel karakterModel = new KarakterModel();
		karakterModel.setNamaKarakter(namaKarakter);
		karakterModel.setLevelKarakter(levelKarakter);
		karakterModel.setStatusKarakter(statusKarakter);
		
		karakterService.create(karakterModel);
		
		model.addAttribute("karakterModelList", karakterModelList);
		
		String html = "karakter/hasil_daftar";
		return html;
	}
	
	@RequestMapping(value="#")
	public String menuTampilKarakter(HttpServletRequest request, Model model) {
		List<KarakterModel> karakterModelList = new ArrayList<KarakterModel>();
		karakterModelList = this.karakterService.read();
		model.addAttribute("karakterModelList", karakterModelList);
		
		String html = "#";
		return html;
	}
}
