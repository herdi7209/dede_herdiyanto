package com.ats.belajarspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HalamanController {

	@RequestMapping(value="linkganti")
	public String methodPanggilGanti() {
		String html ="ganti";
		return html;
	}
	
	@RequestMapping(value="linkbaru")
	public String methodPanggilBaru() {
		String html ="baru";
		return html;
	}
	
	@RequestMapping(value="linkkelompok")
	public String methodPanggilKelompok() {
		String html ="/kelompok/kelompok";
		return html;
	}
	
	@RequestMapping(value="linkCSS")
	public String methodCSS() {
		String html = "/belajarCSS/contohCSS";
		return html;
	}
}
