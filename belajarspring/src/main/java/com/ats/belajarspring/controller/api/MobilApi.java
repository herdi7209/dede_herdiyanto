package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.MobilModel;
import com.ats.belajarspring.repository.MobilRepository;
import com.ats.belajarspring.service.MobilService;

@RestController
@RequestMapping(value="api/MobilApi")
public class MobilApi {

	@Autowired
	private MobilService mobilService;
	
	@Autowired
	private MobilRepository mobilRepository;
	
	@PostMapping("/post")
	@ResponseStatus(code= HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody MobilModel mobilModel){
		mobilService.create(mobilModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Simpan");
		return map;
	}
	
	@GetMapping("/get")
	public List<MobilModel> getApi(){
		List<MobilModel> mobilModelList = new ArrayList<MobilModel>();
		mobilModelList = mobilService.read();
		return mobilModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code= HttpStatus.OK)
	public Map<String, Object> putApi(@RequestBody MobilModel mobilModel){
		mobilService.update(mobilModel);
	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Update");
		return map;
	}
	
	@DeleteMapping("/delete/{kodeMobil}")
	public Map<String, Object> deleteApi(@PathVariable String kodeMobil){
		mobilService.delete(mobilRepository.cariKodeMobil(kodeMobil));
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Data Berhasil di Hapus");
		return map;
	}
	
}
