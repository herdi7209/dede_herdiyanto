package com.ats.belajarspring.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.MotorModel;
import com.ats.belajarspring.service.MotorService;

@Controller
public class MotorController {

	@Autowired
	private MotorService motorService;
	
	@RequestMapping(value="tambah_motor")
	public String menuTambahMotor() {
		String html="motor/tambah_motor";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_motor")
	public String menuHasilTambahMotor(HttpServletRequest request, Model model) {
		String kodeMotor = request.getParameter("kodeMotor");
		String namaMotor = request.getParameter("namaMotor");
		int hargaMotor = Integer.valueOf(request.getParameter("hargaMotor"));
		
		List<MotorModel> motorModelList = new ArrayList<MotorModel>();
		
		MotorModel motorModel = new MotorModel();
		motorModel.setKodeMotor(kodeMotor);
		motorModel.setNamaMotor(namaMotor);
		motorModel.setHargaMotor(hargaMotor);
		
		this.motorService.create(motorModel);
		
		motorModelList = motorService.read();
		
		model.addAttribute("motorModelList", motorModelList);
		
		String html="motor/hasil_tambah_motor";
		return html;
	}
}
