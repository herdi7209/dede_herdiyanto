package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.BioskopModel;
import com.ats.belajarspring.service.BioskopService;

@Controller
public class BioskopController {
 
	@Autowired
	private BioskopService bioskopService;
	
	@RequestMapping(value="tambah_film")
	public String tambahFilm() {
		String html ="bioskop/tambah_film";
		return html;
	}
	
	@RequestMapping(value="tambah_banyak_film")
	public String menuTambahFilm (HttpServletRequest request, Model model) {
		String namaFilm = request.getParameter("namaFilm");
		int ratingFilm = Integer.valueOf(request.getParameter("ratingFilm"));
		
		List<BioskopModel> bioskopModelList = new ArrayList<BioskopModel>();
		
		BioskopModel bioskopModel = new BioskopModel();
		bioskopModel.setNamaFilm(namaFilm);
		bioskopModel.setRatingFilm(ratingFilm);
		
		this.bioskopService.create(bioskopModel);
		
		bioskopModelList = this.bioskopService.read();
		
		model.addAttribute("bioskopModelList", bioskopModelList);
		
		String html="bioskop/hasil_tambah_film";
		return html;
	}
}
