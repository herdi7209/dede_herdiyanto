package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.service.ProdukService;



@Controller
public class ProdukController {

	@Autowired
	private ProdukService produkService;
	
	@RequestMapping(value="menu_tambah_produk")
	public String menuTambahProduk() {
		String html="produk/tambah_produk";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_produk")
	public String prosesTambahProduk(HttpServletRequest request, Model model) {
		String kodeProduk = request.getParameter("kode_produk");
		String namaProduk = request.getParameter("nama_produk");
		int hargaProduk = Integer.valueOf(request.getParameter("harga_produk"));
	
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		
		ProdukModel produkModel = new ProdukModel();
		produkModel.setKodeProduk(kodeProduk);
		produkModel.setNamaProduk(namaProduk);
		produkModel.setHargaProduk(hargaProduk);
		
		produkService.create(produkModel);
	
		produkModelList = produkService.tampilProduk();
		
		model.addAttribute("produkModelList", produkModelList);
		
		String html="produk/success";
		return html;
	}
	
	@RequestMapping(value="tampil_produk")
	public String tampilProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		
		model.addAttribute("produkModelList", produkModelList);
		String html = "produk/tampil_produk";
		return html;
	}
	
	@RequestMapping(value="tampil_nama_produk")
	public String cariProduk(HttpServletRequest request, Model model) {
		String namaProduk = request.getParameter("namaProduk");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariNamaProduk(namaProduk);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampil_produk";
		return html;
	}
	
	@RequestMapping(value="tampil_kode_produk")
	public String cariKode(HttpServletRequest request, Model model) {
		String kodeProduk = request.getParameter("kodeProduk");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariKode(kodeProduk);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampil_produk";
		return html;
	}
	
	@RequestMapping(value="menu_daftar_produk")
	public String menuDaftarProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk";
		return html;
	}
	
	@RequestMapping(value="menu_daftar_produk_ASC")
	public String menuDaftarProdukASC(HttpServletRequest request, Model model) {
		String hargaProduk = request.getParameter("hargaProduk");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariHargaASC(hargaProduk);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk_ASC";
		return html;
	}
}
