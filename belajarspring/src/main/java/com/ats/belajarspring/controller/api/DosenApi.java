package com.ats.belajarspring.controller.api;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.service.DosenService;


@RestController
@RequestMapping("api/DosenApi")
public class DosenApi {

	@Autowired
	private DosenService dosenService;

	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postApi(@RequestBody DosenModel dosenModel){
		this.dosenService.create(dosenModel);
		
	Map<String, Object> map = new HashMap<String, Object>();
	map.put("berhasil", Boolean.TRUE);
	map.put("pesan", "SELAMAT DATA BERHASIL DIMASUKAN");
	return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code=HttpStatus.OK)
	public List<DosenModel> getApi(){ 
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		return dosenModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putApi (@RequestBody DosenModel dosenModel){
		this.dosenService.update(dosenModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Selamat Data Berhasil di Update");
		return map;
	}
	
	@DeleteMapping("/delete/{namaDosen}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteApi(@PathVariable String namaDosen){
		this.dosenService.delete(namaDosen);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("berhasil", Boolean.TRUE);
		map.put("pesan", "Selamat Data Berhasil di Hapus");
		return map;
	}
	
	@GetMapping("/usialessthan")
	@ResponseStatus(code=HttpStatus.OK)
	public List<DosenModel> getUsialessthan(){ 
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.usiaLessDuaLima();
		return dosenModelList;
	}
	
	@GetMapping("/usiamorethan")
	@ResponseStatus(code=HttpStatus.OK)
	public List<DosenModel> getUsiamorethan(){ 
		List<DosenModel> dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.usiaMoreThanLimaPuluh();
		return dosenModelList;
	}
}
