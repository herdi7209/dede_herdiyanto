package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.MobilModel;
import com.ats.belajarspring.service.MobilService;

@Controller
public class MobilController {

	@Autowired
	private MobilService mobilService;
	
	@RequestMapping(value="tambah_mobil")
	public String menuTambahMobil() {
		String html="mobil/tambah_mobil";
		return html;
	}
	
	@RequestMapping(value="proses_tambah_mobil")
	public String menuProsesTambahMobil(HttpServletRequest request, Model model) {
		String kodeMobil = request.getParameter("kodeMobil");
		String namaMobil = request.getParameter("namaMobil");
		int hargaMobil = Integer.valueOf(request.getParameter("hargaMobil"));
		
		List<MobilModel> mobilModelList = new ArrayList<MobilModel>();
		
		MobilModel mobilModel = new MobilModel();
		mobilModel.setKodeMobil(kodeMobil);
		mobilModel.setNamaMobil(namaMobil);
		mobilModel.setHargaMobil(hargaMobil);
		
		this.mobilService.create(mobilModel);
		
		mobilModelList = this.mobilService.read();
		
		model.addAttribute("mobilModelList", mobilModelList);
		
		String html="mobil/hasil_tambah_mobil";
		return html;
	}
	
	@RequestMapping(value="proses_cari_mobil")
	public String prosesCariMobil(HttpServletRequest request, Model model) {
		
		
		String html="hasil_cari_mobil";
		return html;
	}
}
