package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_GURU")
public class GuruModel {

	@Id
	@Column(name="KD_GURU")
	private int kodeGuru;
	
	@Column(name="NM_GURU")
	private String namaGuru;
	
	@Column(name="JRSN_GURU")
	private String jurusanGuru;

	public int getKodeGuru() {
		return kodeGuru;
	}

	public void setKodeGuru(int kodeGuru) {
		this.kodeGuru = kodeGuru;
	}

	public String getNamaGuru() {
		return namaGuru;
	}

	public void setNamaGuru(String namaGuru) {
		this.namaGuru = namaGuru;
	}

	public String getJurusanGuru() {
		return jurusanGuru;
	}

	public void setJurusanGuru(String jurusanGuru) {
		this.jurusanGuru = jurusanGuru;
	}
	
	
}
