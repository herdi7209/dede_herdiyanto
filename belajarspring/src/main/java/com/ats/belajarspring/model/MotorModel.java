package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_MOTOR")
public class MotorModel {

	@Id
	@Column(name="KD_MTR")
	private String kodeMotor;
	
	@Column(name="NM_MTR")
	private String namaMotor;
	
	@Column(name="HRG_MTR")
	private int hargaMotor;

	public String getKodeMotor() {
		return kodeMotor;
	}

	public void setKodeMotor(String kodeMotor) {
		this.kodeMotor = kodeMotor;
	}

	public String getNamaMotor() {
		return namaMotor;
	}

	public void setNamaMotor(String namaMotor) {
		this.namaMotor = namaMotor;
	}

	public int getHargaMotor() {
		return hargaMotor;
	}

	public void setHargaMotor(int hargaMotor) {
		this.hargaMotor = hargaMotor;
	}
	
	
	
	
}
