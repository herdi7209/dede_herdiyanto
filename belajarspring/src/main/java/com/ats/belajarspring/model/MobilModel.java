package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_MBL")
public class MobilModel {

	@Id
	@Column(name="KD_MBL")
	private String kodeMobil;
	
	@Column(name="NM_MBL")
	private String namaMobil;
	
	@Column(name="HRG_MBL")
	private int hargaMobil;
	
	public String getKodeMobil() {
		return kodeMobil;
	}
	public void setKodeMobil(String kodeMobil) {
		this.kodeMobil = kodeMobil;
	}
	public String getNamaMobil() {
		return namaMobil;
	}
	public void setNamaMobil(String namaMobil) {
		this.namaMobil = namaMobil;
	}
	public int getHargaMobil() {
		return hargaMobil;
	}
	public void setHargaMobil(int hargaMobil) {
		this.hargaMobil = hargaMobil;
	}
	
}
