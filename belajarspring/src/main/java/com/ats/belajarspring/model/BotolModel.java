package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_BOTOL")
public class BotolModel {

	@Id
	@Column(name="KD_BTL")
	private String kodeBotol;
	
	@Column(name="NM_BTL")
	private String namaBotol;
	
	@Column(name="HRG_BTL")
	private int hargaBotol;

	public String getKodeBotol() {
		return kodeBotol;
	}

	public void setKodeBotol(String kodeBotol) {
		this.kodeBotol = kodeBotol;
	}

	public String getNamaBotol() {
		return namaBotol;
	}

	public void setNamaBotol(String namaBotol) {
		this.namaBotol = namaBotol;
	}

	public int getHargaBotol() {
		return hargaBotol;
	}

	public void setHargaBotol(int hargaBotol) {
		this.hargaBotol = hargaBotol;
	}
	
	
}
