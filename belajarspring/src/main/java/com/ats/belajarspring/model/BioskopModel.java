package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TBL_BIOSKOP")
public class BioskopModel {
	
	@Id
	@Column(name="CLM_NAMA_FILM")
	private String namaFilm;
	
	@Column(name="CLM_RATING_FILM")
	private int ratingFilm;

	public String getNamaFilm() {
		return namaFilm;
	}

	public void setNamaFilm(String namaFilm) {
		this.namaFilm = namaFilm;
	}

	public int getRatingFilm() {
		return ratingFilm;
	}

	public void setRatingFilm(int ratingFilm) {
		this.ratingFilm = ratingFilm;
	}
	
	
	
	

}
