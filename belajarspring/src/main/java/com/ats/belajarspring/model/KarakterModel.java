package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_KARAKTER")
public class KarakterModel {

	@Id
	@Column(name="NAMA_KARAKTER")
	private String namaKarakter;
	
	@Column(name="LEVEL_KARAKTER")
	private int levelKarakter;
	
	@Column(name="STATUS_KARAKTER")
	private String statusKarakter;
	
	public String getNamaKarakter() {
		return namaKarakter;
	}
	public void setNamaKarakter(String namaKarakter) {
		this.namaKarakter = namaKarakter;
	}
	public int getLevelKarakter() {
		return levelKarakter;
	}
	public void setLevelKarakter(int levelKarakter) {
		this.levelKarakter = levelKarakter;
	}
	public String getStatusKarakter() {
		return statusKarakter;
	}
	public void setStatusKarakter(String statusKarakter) {
		this.statusKarakter = statusKarakter;
	}
	
	
	
	
}
